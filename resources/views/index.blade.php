<html>
<body>
  こんにちは{{ Session::get('userlog') }}様
  <table border="1">
    <tr>
      <th>id</th>
      <th>name</th>
      <th>email</th>
      <th>password</th>
      <th>修正</th>
      <th>削除</th>
    </tr>
    @foreach ($items as $item)
    <tr>
      <td>{{$item->id}}</td>
      <td>{{$item->name}}</td>
      <td>{{$item->email}}</td>
      <td>{{$item->password}}</td>
      <form method="post" action="{{ action('AdminUserController@edit', $item -> id) }}">
        {{ csrf_field() }}
      <td><input type="submit" value="修正"></td>
    </form>
      <form method="post" action="{{ action('AdminUserController@delete_confirm', $item -> id) }}">
        {{ csrf_field() }}
      <td><input type="submit" value="削除"></td>
    </form>
    </tr>
    @endforeach
  </table>
</body>
</html>
