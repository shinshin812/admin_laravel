<html>
<body>
  <form method="post" action="{{ url('record_confirm') }}">
    {{ csrf_field() }}
    @if ($errors->any())
<div class="errors">
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
    <div class="form-item">名前</div>
    <input type="text" name="name" value="{{ old('name') }}">
    <div class="form-item">メールアドレス</div>
    <input type="text" name="email" value="{{ old('email') }}">
    <div class="form-item">パスワード</div>
    <input type="text" name="password" value="{{ old('password') }}">
    <input type="submit" value="登録する">
  </form>
</body>
</html>
