<html>
<body>
  <form method="post" action="{{ action('AdminUserController@edit_complete', $item -> id) }}">
    {{ csrf_field() }}
    @if ($errors->any())
<div class="errors">
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
  <table border="1">
    <tr>
      <td>id</td>
      <td>name</td>
      <td>email</td>
      <td>password</td>
      <td>修正</td>
    </tr>
    <tr>
      <input type="hidden" value="{{$item->id}}" name="id">
      <td>{{$item->id}}</td>
      <td><input type="text" value="{{$item->name}}" name="name"></td>
      <td><input type="text" value="{{$item->email}}" name="email"></td>
      <td><input type="text" value="{{$item->password}}" name="password"></td>
      <td><input type="submit" value="修正"></td>
    </tr>
  </table>
</form>
</body>
</html>
