<html>
<body>
  <form method="post" action="{{ action('AdminUserController@delete_complete', $item -> id) }}">
    {{ csrf_field() }}
    <div class="message">本当に削除しますか?</div>
    <table border="1">
      <tr>
        <td>id</td>
        <td>name</td>
        <td>email</td>
        <td>password</td>
        <td>削除</td>
      </tr>
      <tr>
        <input type="hidden" value="{{$item->id}}" name="id">
        <td>{{$item->id}}</td>
        <input type="hidden" value="{{$item->name}}" name="name">
        <td>{{$item->name}}</td>
        <input type="hidden" value="{{$item->email}}" name="email">
        <td>{{$item->email}}</td>
        <input type="hidden" value="{{$item->password}}" name="password">
        <td>{{$item->password}}</td>
        <td><input type="submit" value="削除"></td>
      </tr>
    </table>
  </form>
</body>
</html>
