<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('index','AdminUserController@index');

Route::get('record','AdminUserController@record');
Route::post('record_confirm','AdminUserController@record_confirm');
Route::post('record_complete','AdminUserController@record_complete');

Route::post('delete_confirm/{id}','AdminUserController@delete_confirm');
Route::post('delete_complete','AdminUserController@delete_complete');

Route::get('edit/{id}','AdminUserController@edit');
Route::post('edit/{id}','AdminUserController@edit');
Route::post('edit_complete','AdminUserController@edit_complete');

Route::get('login','AdminUserController@login');
Route::post('login','AdminUserController@login_validation');
Route::post('index','AdminUserController@index');

Route::get('logout','AdminUserController@logout');
