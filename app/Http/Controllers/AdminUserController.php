<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\AdminUser;

class AdminUserController extends Controller
{
    //

    public function index(Request $request){
      $userlog = $request->session()->get('login_name');
      if(empty($userlog)){
        if($userlog == ""){
          return redirect('login');
      }
    }
      echo  '<a href="http://localhost/admin_laravel/public/logout">ログアウトする</a>';
      // AdminUserクラスのallメソッドで全てのレコードを取得し、$itemsに代入
      $items = AdminUser::all();
      // $itemsをindexへ配列で渡す
      return view('index',['items' => $items]);
    }

    public function record(Request $request){
      echo  '<a href="http://localhost/admin_laravel/public/login">ログインはこちら</a>';
    return view('record');
  }

  public function record_confirm(Request $request){
    $request->validate([
        'name' => 'required',
        'email' => 'required',
        'password' => 'required',
      ]);
    $data = $request->name;
    $data1 = $request->email;
    $data2 = $request->password;
    $request->session()->put('data',$data);
    $request->session()->put('data1',$data1);
    $request->session()->put('data2',$data2);
    $back = $_SERVER['HTTP_HOST'];
      if (!empty($_SERVER['HTTP_REFERER']) && (strpos($_SERVER['HTTP_REFERER'],$back) !== false)){
        echo  '<a href="' . $_SERVER['HTTP_REFERER'] . '">戻る</a>';
      }
    return view('record_confirm', ['data' => $data,'data1' => $data1,'data2' => $data2]);
  }

  public function record_complete(Request $request){
    $data = $request->name;
    $data1 = $request->email;
    $data2 = $request->password;
    $user = $request->session()->get('data',array('data'=>$data));
    $user1 = $request->session()->get('data1',array('data1'=>$data1));
    $user2 = $request->session()->get('data2',array('data2'=>$data2));

    $AdminUser = new AdminUser();
    $AdminUser->name = $user;
    $AdminUser->email = $user1;
    $AdminUser->password = $user2;
    $AdminUser->save();
    $back = $_SERVER['HTTP_HOST'];
      if (!empty($_SERVER['HTTP_REFERER']) && (strpos($_SERVER['HTTP_REFERER'],$back) !== false)){
        echo  '<a href="http://localhost/admin_laravel/public/record">戻る</a>';
    }
    // １回限りのメッセージを表示。フラッシュメッセージ
    $request->session()->flash('message', '登録が完了しました');
    return view('record_complete',compact('user','user1','user2'));
  }

  public function delete_confirm(Request $request){
    $item = AdminUser::find($request->id);
    return view('delete_confirm', ['item' => $item]);
  }

  public function delete_complete(Request $request){
    AdminUser::find($request->id)->delete();
    $back = $_SERVER['HTTP_HOST'];
      if (!empty($_SERVER['HTTP_REFERER']) && (strpos($_SERVER['HTTP_REFERER'],$back) !== false)){
        echo  '<a href="http://localhost/admin_laravel/public/index">戻る</a>';
    }
    return view('delete_complete');
  }

  public function edit(Request $request){
    $item = AdminUser::find($request->id);
    return view('edit', ['item' => $item]);
  }

  public function edit_complete(Request $request){
    $request->validate([
      'name' => 'required',
      'email' => 'required',
      'password' => 'required',
    ]);
    $item = AdminUser::find($request->id);
       $item->name = $request->name;
       $item->email = $request->email;
       $item->password = $request->password;
       $item->save();
    $back = $_SERVER['HTTP_HOST'];
      if (!empty($_SERVER['HTTP_REFERER']) && (strpos($_SERVER['HTTP_REFERER'],$back) !== false)){
        echo  '<a href="http://localhost/admin_laravel/public/index">戻る</a>';
    }
    return view('edit_complete');
  }

  public function login(Request $request){
    echo  '<a href="http://localhost/admin_laravel/public/record">登録はこちら</a>';
    return view('login');
  }

  public function login_validation(Request $request){
    $request->validate([
      'name' => 'required',
      'email' => 'required',
      'password' => 'required',
    ]);
    // 指定した条件で取得。第一引数には条件付けを行うフィールド、第二引数には条件となる値
    $item = AdminUser::where('name',$request->name)->exists();
    $item1 = AdminUser::where('email',$request->email)->exists();
    $item2 = AdminUser::where('password',$request->password)->exists();
    $data = $request->name;
    $data1 = $request->email;
    $data2 = $request->password;
    $request->session()->put('login_name',$data);
    $userlog = $request->session()->get('login_name');
    if($item == $data){
      if($item1 == $data1){
        if($item == $data2){
          return redirect('index')->with(['userlog' => $userlog]);
        }
      }
    }
    if($item != $data){
      echo "入力が間違っています";
      return view('login');
    }
    if($item1 != $data1){
      echo "入力が間違っています";
      return view('login');
    }
    if($item2 != $data2){
      echo "入力が間違っています";
      return view('login');
    }
  }

  public function logout(Request $request){
    $back = $_SERVER['HTTP_HOST'];
    if (!empty($_SERVER['HTTP_REFERER']) && (strpos($_SERVER['HTTP_REFERER'],$back) !== false)){
      echo  '<a href="http://localhost/admin_laravel/public/login">ログインはこちら</a>';
  }
  $request->session()->flush();
    return view('logout');
  }
}
