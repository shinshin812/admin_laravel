<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminUser extends Model
{
    //
    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected $fillable = ['name', 'email','password'];

    protected $table = 'admin_users';
}
